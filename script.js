const scrollToItem = function(className, item, num) {
    $(className).click(() => {
        $('html, body').animate({
            scrollTop: $(item).offset().top -= num
        }, 1000);
    })
}
const hideItem = function(className, item) {
    $(className).click(function(){
        $(item).slideToggle("slow")
    })
}
$(document).ready( () => [
    scrollToItem(".posts-article", ".main", 48),
    scrollToItem(".clients-article", ".popular-clients", 0),
    scrollToItem(".top-rated-article", ".top-rated", 48),
    scrollToItem(".news-article", ".hot-news", 48),
    $(window).scroll(function(){
        if ($(this).scrollTop() > 100) {
            $('.up-btn').fadeIn();
        } else {
            $('.up-btn').fadeOut();
        }
    }),
    scrollToItem(".up-btn", ".nav", 0),
    hideItem(".hide-top-rated", ".top-rated-container"),
    hideItem(".hide-news", ".section-2")
])


 
